#include <stdio.h>

enum read_status{
    READ_SUCCESS,
    READ_FAILED
};
enum read_status open_for_read(char *path,FILE **file);

enum write_status {
    WRITE_SUCCESS=0,
    WRITE_FAILED
};
enum write_status open_for_write(char *path,FILE **file);

enum close_status {
    CLOSE_SUCCESS= 0,
    CLOSE_FAILED
};

enum close_status close_file(FILE* file);



