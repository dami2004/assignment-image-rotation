#include "../include/file_actions.h"
#include "../include/bmp.h"
#include "../include/img.h"
#include "../include/transform.h"
#include "../include/util.h"
#include <stdio.h>
#include <sysexits.h>

int ERROR_CODE=0;

void clear_memory_if_error(FILE* source_image, FILE* transformed_image){
    close_file(source_image);
    close_file(transformed_image); 
    free(source_image);
    free(transformed_image);
}


int main(int argc, char **argv) {
    if (argc != 3) {
        perror("Your input should look like this: ./image-transformer <source-image> <transformed-image>");
        return ERROR_CODE;
    }

    char *source_image_name = argv[1];
    char *transformed_image_name = argv[2];

    FILE *source_image, *transformed_image;
    enum read_status open_file = open_for_read(source_image_name,&source_image);
    if (open_file == READ_FAILED) {
        perror("source image file doesn't exist");
        return ERROR_CODE;
    }

    enum write_status open_copy = open_for_write(transformed_image_name,&transformed_image);
    if (open_copy == WRITE_FAILED) {
        perror("transformed image file doesn't exist");
        clear_memory_if_error(source_image,transformed_image);
        return ERROR_CODE;
    }

    struct image *source = malloc(sizeof(struct image));

    if(source==NULL){
        perror("No source for reading from bmp");
        clear_memory_if_error(source_image,transformed_image);
        return ERROR_CODE;
    }

    enum bmp_read_status status = from_bmp(source_image, source);

    if (status) {
        clear_memory_if_error(source_image,transformed_image);
        free(source -> data);
        perror("error while reading image");
        return ERROR_CODE;
    }

    struct image transformed = transform_rotate(*source);
    free(source -> data);
    free(source);

    if (transformed.data == NULL){
        perror("Failed to transform the picture");
        clear_memory_if_error(source_image,transformed_image);
        return ERROR_CODE;
    }
    
    to_bmp(transformed_image, &transformed);

    free(transformed.data);
    fclose(source_image);
    fclose(transformed_image);
    return 0;
}


