#include "../include/img.h"


struct image new_image(const uint64_t width,const uint64_t height) {
    return (struct image){
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * (width * height))};
}


void clear_image_mem(struct image image){
    free(image.data);
    image.data=NULL;    
}

