#include "../include/bmp.h"
#include "../include/img.h"
#include "../include/util.h"
#include <inttypes.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};



uint16_t get_bf_type(struct  bmp_header* bmpHeader){
    return bmpHeader->bfType;
}

uint32_t get_bi_width(struct  bmp_header* bmpHeader){
    return bmpHeader->biWidth;
}

uint32_t get_bi_height(struct  bmp_header* bmpHeader){
    return bmpHeader->biHeight;
}

uint16_t get_bi_bit_count(struct  bmp_header* bmpHeader){
    return bmpHeader->biBitCount;
}

void set_bf_type( struct bmp_header* bmpHeader, uint16_t value){
    bmpHeader->bfType=value;
}

void set_bi_width( struct bmp_header* bmpHeader,uint32_t value){
    bmpHeader->biWidth=value;
}

void set_bi_height(struct  bmp_header* bmpHeader,uint32_t value){
    bmpHeader->biHeight=value;
}

void set_bi_bit_count(struct  bmp_header* bmpHeader,uint16_t value){
    bmpHeader->biBitCount=value;
}

void set_bf_reserved(struct bmp_header* bmpHeader, uint32_t value){
    bmpHeader -> bfReserved = value;
}

void set_b_off_bits(struct bmp_header* bmpHeader, uint32_t value){
    bmpHeader -> bOffBits = value;
}

void set_bi_size(struct bmp_header* bmpHeader, uint32_t value){
    bmpHeader -> biSize = value;
}

void set_bi_planes(struct bmp_header* bmpHeader, uint16_t value){
    bmpHeader -> biPlanes = value;
}

void set_b_file_size(struct bmp_header* bmpHeader, uint32_t value){
    bmpHeader -> bfileSize = value;
}

enum bmp_read_status from_bmp( FILE* in, struct image* img) {
    struct bmp_header bmpHeader;

    if (!fread(&bmpHeader,sizeof(bmpHeader),1,in) ) { 
        error_out("File header is invalid");
        return INVALID_HEADER;
    }

    if (get_bi_bit_count(&bmpHeader) != BITS_PER_PIXEL) {
        error_out("Number of bits is invalid");
        return BIT_NUMBER_ERROR;
    }

    if (get_bf_type(&bmpHeader) != BMP_SIGNATURE) {
        error_out("File signature is invalid");
        return INVALID_SIGNATURE;
    }

    *img = new_image(get_bi_width(&bmpHeader), get_bi_height(&bmpHeader));
    if(img->data==NULL){
        return READING_ERROR;
    }
    
    int64_t padding = 4 - (get_bi_width(&bmpHeader)*3)%4;

    for (size_t i = 0; i <get_bi_height(&bmpHeader); i=i+1) {
        if (fread((img->data)+i*get_bi_width(&bmpHeader),sizeof(struct pixel) * get_bi_width(&bmpHeader),1,in)==0) {
            error_out("Pixels read fail");
            clear_image_mem(*img);
            return PIXEL_ERROR;
        }
        if (fseek(in,padding,SEEK_CUR) !=0) { 
            error_out("Pixels read fail");
            clear_image_mem(*img);
            return PIXEL_ERROR;
        }
    }
    return BMP_READ_SUCCESS;
}


enum bmp_write_status to_bmp(FILE *out, struct image const* img) { 

    uint64_t w = img->width;
    uint64_t h =img->height;
    struct bmp_header bmpHeader = {0};

    set_bf_type(&bmpHeader, BMP_SIGNATURE);
    set_b_file_size(&bmpHeader, sizeof(struct bmp_header) + (h) * (sizeof(struct pixel) * (w) + 4 - (w*3)%4));

    set_bf_reserved(&bmpHeader, 0);
    set_b_off_bits(&bmpHeader,sizeof(struct bmp_header));
    set_bi_size(&bmpHeader, BITMAPINFOHEADER);
    set_bi_width(&bmpHeader, w);
    set_bi_height(&bmpHeader, h);
    set_bi_planes(&bmpHeader, PLANES_NUMBER);
    set_bi_bit_count(&bmpHeader, BITS_PER_PIXEL);


    size_t count = fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out);
    if (count != 1) {
        return ERROR;
        error_out("Couldnt read from bmp");
    }

    uint32_t padding = 4 - (w*3)%4;

    for (size_t i = 0; i < h; i=i+1) {
        count = fwrite(img->data + i * w, sizeof(struct pixel), w, out);
        if (count != w) {
            return ERROR;
        }

        uint32_t tmp = 0;
        size_t res=fwrite(&tmp, 1, padding, out);
        if (res == EOF) {
                return ERROR;
            }
        // for (size_t j = 0; j < padding; j=j+1) {
        //     int res = fputc(0, out);
        //     if (res == EOF) {
        //         return ERROR;
        //     }
        // }
    }

    return BMP_WRITE_SUCCESS;
}
