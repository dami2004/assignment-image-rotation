#include "../include/file_actions.h"

enum read_status open_for_read(char *path,FILE **file) {
    *file = fopen(path, "rb");
    if (file == NULL){
        return READ_FAILED;
    }
    return READ_SUCCESS;
}

enum write_status open_for_write( char *path,FILE **file) {
    *file = fopen(path, "wb");
    if (file == NULL){
        return WRITE_FAILED;
    }
    return WRITE_SUCCESS;

}



enum close_status close_file(FILE* file) {
    if (fclose(file) == 0) return CLOSE_SUCCESS;
    return CLOSE_FAILED;
}
